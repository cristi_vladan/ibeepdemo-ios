//
//  ViewController.m
//  iBeepDemo
//
//  Created by Cristi Vladan on 22/01/15.
//  Copyright (c) 2015 Logic Soft. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController 

__weak UIImage *tokenFoundImage = nil;
__weak UIImage *tokenNotFoundImage = nil;
__weak UIImage *tokenUndefinedImage = nil;


- (void)viewDidLoad {
    [super viewDidLoad];
    tokenFoundImage = [UIImage imageNamed: @"green_circle.png"];
    tokenNotFoundImage = [UIImage imageNamed: @"dark_red_circle.png"];
    tokenUndefinedImage = [UIImage imageNamed: @"dark_orange_circle.png"];
    IBeep *_iBeep = [IBeep getInstance];
    [_iBeep setIBeepDelegate:self];
    [IBeep enableIBeaconScan:NO];
    [IBeep setApiKey:@"8FRmtjedlRybqKh"];
    //[IBeep stopDetectionService];
    [IBeep startDetectionService:@"cristivladan@gmail.com" callback:^(BOOL success){
            NSLog(success ? @"started successfully" : @"start failed");
    }];
    // Do any additional setup after loading the view, typically from a nib.
    

    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) beaconDetected : (NSString *) beaconId beaconName : (NSString *) beaconName locationId : (NSString *) locationId
{
}

- (void) tokenDetected : (NSString *) token channel : (int) channel
{
    dispatch_async(dispatch_get_main_queue(), ^{ //update UI only on main thread
        
        [images[(int)channel] setImage:tokenFoundImage];
        UILabel *label = (UILabel *)[tokens objectAtIndex:(int)channel];
        [label setText:token];
        UILabel *hitsLabel = (UILabel *)[hits objectAtIndex:(int) channel];
        hitsLabel.text =[NSString stringWithFormat:@"%d", hitsLabel.text.intValue + 1];
        NSLog(@"hits = %d", hitsLabel.text.intValue);
        

    });
    /*
    double delayInSeconds = 5.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        for (int ch = 0 ; ch < 3 ; ch ++) {
            [images[(int)ch] setImage:tokenNotFoundImage];
        }

    });
     */
    NSLog(@"tokenDetected = %@", token);

}
- (void) partialTokenDetected : (NSString *) partialToken channel : (int) channel
{
    dispatch_async(dispatch_get_main_queue(), ^{ //update UI only on main thread
        [images[(int)channel] setImage:tokenUndefinedImage];
        UILabel *label = (UILabel *)[partialTokens objectAtIndex:(int)channel];
        [label setText:partialToken];
    });

    
    NSLog(@"partial token = %@", partialToken);

}

@end
