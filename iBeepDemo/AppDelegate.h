//
//  AppDelegate.h
//  iBeepDemo
//
//  Created by Cristi Vladan on 22/01/15.
//  Copyright (c) 2015 Logic Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iBeep/iBeep.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

