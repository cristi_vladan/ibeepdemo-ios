//
//  ViewController.h
//  iBeepDemo
//
//  Created by Cristi Vladan on 22/01/15.
//  Copyright (c) 2015 Logic Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iBeep/iBeep.h>
#import <CoreBluetooth/CoreBluetooth.h>


@interface ViewController : UIViewController <IBeepProtocolDelegate> {
    IBOutletCollection(UIImageView) NSArray *images;
    IBOutletCollection(UILabel) NSArray *hits;
    IBOutletCollection(UILabel) NSArray *tokens;
    IBOutletCollection(UILabel) NSArray *partialTokens;

}
@property (strong, retain) CBCentralManager *bluetoothManager;

@property (nonatomic, weak) id <IBeepProtocolDelegate> delegate;


@end

